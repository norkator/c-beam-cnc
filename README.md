# C-Beam CNC

Budget CNC router project files, 3D models, firmware etc under one repository.

I build this as a hobby and this repo is for my own use to keep track of whats going on and see later what was going on and in which point.

Another reason is that anyone could just clone is repo and build he's/her's own just like that without huge amount of work to dig information. That information digging part is my job here.

Happy machining!