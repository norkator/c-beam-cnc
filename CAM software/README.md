
# C-Beam Project CAM software

In this section I added extensions, links and details related to suitable CAM software. Free, easy and cheap to get started faster.


## SketchUcam

`SketchUcam-1_4d-80c0152.rbz`

Handy extension for creating simple milling projects. Outputs gcode.


## CAMotics

Added link about CAMotics which is gcode plotter / simulator software. Handy tool for visualizing SketchUcam gcode output.

